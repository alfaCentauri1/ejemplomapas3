package com.alfaCentauri;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ejecutable {
    public static void agregandoDatosPruebas(Map entrada) {
        if (entrada != null){
            entrada.put("dato1", "este es un ejemplo");
            entrada.put("numero", 25.3);
        }
    }

    /**
     * @param jsonInput  Type Object.
     * @return Return a arraylist with data or arraylist empty.
     **/
    public static  List validateArray(Object jsonInput) {
        List detalles = new ArrayList();
        if ( jsonInput != null ) {
            if (jsonInput instanceof ArrayList) {
                detalles = (ArrayList)jsonInput;
            } else {
                detalles.add((Map)jsonInput);
            }
        }
        return detalles;
    }

    /**
     * Ejemplo de agrupado con map.
     * @param args
     */
    public static void main(String[] args) {
        Map mapa = new HashMap();
        Map encabezado = new HashMap();
        agregandoDatosPruebas(mapa);
        /* Debug */
        System.out.println("La entrada: " + mapa);
        System.out.println("El encabezado: " + encabezado);
        /***/
        String codigo = "", codigoLote = "";
        Integer cantidad;
        List detalle = validateArray( mapa.get("Detalle") );
        List toAgrup = new ArrayList();
        List subDetalles;
        Map agrup = new LinkedHashMap();
        /* Inicia el contador de lineas LX */
        int counter=1;
        /* Recorre el jsonArray de entrada */
        for ( Object item : detalle ) {
            Map element = (Map) item;
            codigo = (String)element.get("codigo");
            cantidad = (Integer)element.get("CantidadPickeada");
            codigoLote = (String)element.get("codigoLote");
            String numeroLinea = (String)element.get("numeroLinea");
            String keyName = codigo + numeroLinea;
            Object find = agrup.get(keyName);//codigo+lineExterna
            /* Existe el codigo. Guardar en un nuevo arreglo de detalles */
            if (find != null) {
                Map newItem = (HashMap) agrup.get(codigo);
                List listado = (ArrayList)newItem.get("subDetalles");

                /* codigoLote es diferente?*/
                boolean isValidToAgrup = true;
                for (Object subElement : listado) {
                    Map currentItem = (Map) subElement;
                    String lote_subElement = (String) currentItem.get("codigoLote");
                    if ( codigoLote.equals(lote_subElement) ) {
                        isValidToAgrup = false;
                        break;
                    }
                }
                if (isValidToAgrup) {
                    Map elementSubDetalle = new HashMap();
                    elementSubDetalle.put("codigoLote", codigoLote);
                    elementSubDetalle.put("cantidad", cantidad);
                    elementSubDetalle.put("codigo", codigo);
                    listado.add(0, elementSubDetalle);
                }
                else {
                    /* nuevo key con codigo+numeroLinea+lote */
                    numeroLinea = (String)element.get("numeroLinea");

                    List listadoNuevo = new ArrayList();
                    newItem = new HashMap();
                    Map elementSubDetalle = new HashMap();
                    elementSubDetalle.put("codigoLote", codigoLote);
                    elementSubDetalle.put("cantidad", cantidad);
                    elementSubDetalle.put("codigo", codigo);
                    listadoNuevo.add(0, elementSubDetalle);

                    newItem.put("numeroLinea", numeroLinea);
                    newItem.put("conteoSequencial", counter);
                    newItem.put("subDetalles", listadoNuevo);
                    newItem.put("identificador", encabezado.get("identificador"));
                    newItem.put("identificador_2", encabezado.get("identificador_2"));
                    agrup.put( keyName + codigoLote, newItem );
                    counter++;
                }
            }
            else {
                /* No existe y se crea uno nuevo */
                numeroLinea = (String)element.get("numeroLinea");

                List listadoNuevo = new ArrayList();
                Map newItem = new HashMap();
                Map elementSubDetalle = new HashMap();
                elementSubDetalle.put("codigoLote", codigoLote);
                elementSubDetalle.put("cantidad", cantidad);
                elementSubDetalle.put("codigo", codigo);
                listadoNuevo.add(0, elementSubDetalle);

                newItem.put("numeroLinea", numeroLinea);
                newItem.put("conteoSequencial", counter);
                newItem.put("subDetalles", listadoNuevo);
                newItem.put("identificador", encabezado.get("identificador"));
                newItem.put("identificador_2", encabezado.get("identificador_2"));
                agrup.put(keyName, newItem);
                counter++;
            }
        }
        //
        System.out.println("agrup: " + agrup);

        List nDetalle = new ArrayList();
        for (Object key : agrup.keySet()) {
            Map data = (HashMap)agrup.get(key);
            Map aux = new HashMap();
            aux.put("conteoSequencial", data.get("conteoSequencial"));
            aux.put("numeroLinea", data.get("numeroLinea"));
            aux.put("subDetalles", data.get("subDetalles"));
            aux.put("identificador", data.get("identificador"));
            aux.put("identificador_2", data.get("identificador_2"));
            aux.put("N9", data.get("N9"));
            nDetalle.add(0, aux);
        }

        mapa.remove("Detalle");
        mapa.put("nDetalle", nDetalle);
        /* Debug */
        System.out.println("El nuevo detalle " + nDetalle);
        System.out.println("El resultado es: " + encabezado);
    }
}
