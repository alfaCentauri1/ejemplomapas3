package com.alfaCentauri;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Ejecutable2 {

    /**
     * @param jsonInput  Type Object.
     * @return Return arraylist with data or arraylist empty.
     **/
    public static List validateArray(Object jsonInput) {
        List detalles = new ArrayList();
        if ( jsonInput != null ) {
            if (jsonInput instanceof ArrayList) {
                detalles = (ArrayList)jsonInput;
            } else {
                detalles.add((Map)jsonInput);
            }
        }
        return detalles;
    }

    /**
     * @param aux Type Map.
     * @return Return a date on formatt string.
     **/
    public static String transformDateLongToString(Object aux){
        String result = "";
        if ( aux instanceof Map ) {
            Map dateTypeLong = (Map)aux;
            Long numberLong = (Long) dateTypeLong.getOrDefault("long",null); //get("long");
            if (numberLong != null) {
                Date inDate = new Date(TimeUnit.SECONDS.toMillis(numberLong));
                result = inDate.toString();
            }
        }
        else {
            if (aux instanceof Long)
                result = Long.toString( (Long) aux);
        }
        return result;
    }

    /**
     * @param aux Type Map.
     * @return Return a date on formatt string.
     **/
    public static String transformDateLongToString_2(Object aux){
        String result = "";
        /* debug */
        System.out.println( "Entrada al transformDateLongToString: " + aux.toString() );
        if ( aux instanceof Map ) {
            Map dateTypeLong = (Map) aux;
            result = getDateLongToStringFormatt( (Long) dateTypeLong.get("long") );
        }
        else if (aux instanceof Long) {
            result = getDateLongToStringFormatt( (Long) aux );
        }
        else if (aux instanceof String) {
            result = (String) aux;
        }
        return result;
    }

    /**
     *
     * @param numberLong Type long.
     * @return Return a string with date on formatt "yyyy-MM-dd'T'HH:mm:ss" or empty.
     **/
    private static String getDateLongToStringFormatt(Long numberLong) {
        String StringDate = "";
        SimpleDateFormat formatDay;
        if (numberLong != null) {
            Date inDate = new Date(TimeUnit.SECONDS.toMillis(numberLong));
            formatDay = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            StringDate = formatDay.format(inDate);
        }
        return StringDate;
    }

    /**
     * Ejemplo de agrupado con map.
     * @param args Type array string.
     */
    public static void main (String []args) {
        System.out.println("Ejemplo de Map con fechas en formato long.");
        Map mapa = new HashMap();
        List arregloDatos = new ArrayList();
        //LinkedHashMap arregloDatos = new LinkedHashMap<>();
        /* Lista de Hash map */
        long fechaLong = 1691578800L;
        for (int i=0; i < 4; i++) {
            Map newMap = new HashMap<>();
            newMap.put("title", "Prueba #"+i);
            newMap.put("FechaVencimiento", fechaLong);
            fechaLong = fechaLong + 2;
            Map fechaFabricacion = new HashMap<>();
            fechaFabricacion.put("long", 123456L);
            newMap.put("FechaFabricacion",fechaFabricacion);
            arregloDatos.add(newMap);
        }
        /* data de pruebas */
        mapa.put("nombre", "Pedro");
        mapa.put("apellido","Perez");
        mapa.put("Edad",35);
        mapa.put("Detalles", arregloDatos);
        List listado = validateArray( mapa.get("Detalles") );
        /* Debug */
        System.out.println("La entrada: " + mapa);
        for ( Object item : listado ) {
            Map element = (Map) item;
            Object auxDate = element.get("FechaVencimiento");
            String fechaVencimiento = transformDateLongToString_2( auxDate );
            /* debug */
            System.out.println( "FechaVencimiento con formato: " + fechaVencimiento );
            /* Fecha fabricación */
            auxDate = element.get("FechaFabricacion");
            String fechaFabricacion = transformDateLongToString_2( auxDate );
            System.out.println( "FechaFabricacion: " + fechaFabricacion );
        }
        System.out.println("Fin del Ejemplo.");
    }
}
