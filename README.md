# EjemploMapas3

Ejemplos del uso de Maps en Java. Recorrer un json Array e imprimirlo. Transformar fechas de un formato long (ISO) a una 
cadena de caracteres con formato legible.

## Contenido
<!-- TOC -->
* [EjemploMapas3](#ejemplomapas3)
  * [Contenido](#contenido)
  * [Instalación](#instalacin)
  * [Authors and acknowledgment](#authors-and-acknowledgment)
  * [License](#license)
  * [Project status](#project-status)
<!-- TOC -->

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ejecute desde la consola; en la raíz del proyecto, java Ejecutable.jar

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer